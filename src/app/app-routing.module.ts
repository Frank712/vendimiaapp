import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { AddarticuloComponent } from './articulos/addarticulo/addarticulo.component';
import { EditarticuloComponent } from './articulos/editarticulo/editarticulo.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { ClientesComponent } from './clientes/clientes.component';
import { AddclienteComponent } from './clientes/addcliente/addcliente.component';
import { EditclienteComponent } from './clientes/editcliente/editcliente.component';
import { VentaComponent } from './venta/venta.component';
import { AddventaComponent } from './venta/addventa/addventa.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'ventas', component: VentaComponent },
  { path: 'registro-venta/:id', component: AddventaComponent },
  { path: 'clientes', component: ClientesComponent },
  { path: 'registro-cliente/:id', component: AddclienteComponent },
  { path: 'editar-cliente/:id', component: EditclienteComponent },
  { path: 'configuracion', component: ConfiguracionComponent },
  { path: 'articulos', component: ArticulosComponent },
  { path: 'editar-articulo/:id', component: EditarticuloComponent },
  { path: 'registro-articulo/:id', component: AddarticuloComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
