import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatAutocompleteModule,MatInputModule } from '@angular/material';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { ClientesComponent } from './clientes/clientes.component';
import { VentaComponent } from './venta/venta.component';
import { MainComponent } from './main/main.component';
import { AddclienteComponent } from './clientes/addcliente/addcliente.component';
import { EditclienteComponent } from './clientes/editcliente/editcliente.component';
import { AddarticuloComponent } from './articulos/addarticulo/addarticulo.component';
import { EditarticuloComponent } from './articulos/editarticulo/editarticulo.component';
import { AddventaComponent } from './venta/addventa/addventa.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ArticulosComponent,
    ConfiguracionComponent,
    ClientesComponent,
    VentaComponent,
    MainComponent,
    AddclienteComponent,
    EditclienteComponent,
    AddarticuloComponent,
    EditarticuloComponent,
    AddventaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatAutocompleteModule, 
    MatInputModule
  ],
  exports: [MatAutocompleteModule, MatInputModule],
  providers: [{provide: LOCALE_ID, useValue: 'es-MX'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
