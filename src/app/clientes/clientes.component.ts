import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  clientes : any [] = [];
  id: any;

  constructor(private service: ServiceService) { }

  ngOnInit() { 
    this.service.getClientes().subscribe(clientes => {
      console.log(clientes);
      for (const id$ in clientes){
        const c = clientes[id$];
        c.id$ = id$;
        this.clientes.push(clientes[id$]);
      }
      this.id = Number(this.clientes.length) + 1;
    })
    
  }

}
