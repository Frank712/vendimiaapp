import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ServiceService } from '../../service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-addcliente',
  templateUrl: './addcliente.component.html',
  styleUrls: ['./addcliente.component.css']
})
export class AddclienteComponent implements OnInit {
  cliente : any;
  clienteForm: FormGroup;
  cliente_id: any;
  constructor(private cf: FormBuilder, private actiavatedRouter: ActivatedRoute, 
    private service: ServiceService, private router : Router) { }

  ngOnInit() {
    this.clienteForm = this.cf.group({
      nombre: new FormControl('', Validators.required),
      ap_paterno: new FormControl('', Validators.required),
      ap_materno: new FormControl(''),
      rfc: new FormControl('', Validators.required)
    });
    this.cliente_id = this.actiavatedRouter.snapshot.paramMap.get('id');
  }

  postCliente() {
    this.cliente = this.saveCliente();
    console.log(this.cliente);
    this.service.postCliente(this.cliente).subscribe(resp => {
      this.router.navigate(['clientes']);
    });
  };

  saveCliente() {
    const saveConfig = {
      clave: this.cliente_id,
      nombre: this.clienteForm.get('nombre').value,
      ap_paterno: this.clienteForm.get('ap_paterno').value,
      ap_materno: this.clienteForm.get('ap_materno').value,
      rfc: this.clienteForm.get('rfc').value
    }
    return saveConfig;
  }




}
