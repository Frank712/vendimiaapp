import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ServiceService } from '../../service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editcliente',
  templateUrl: './editcliente.component.html',
  styleUrls: ['./editcliente.component.css']
})
export class EditclienteComponent implements OnInit {
  cliente : any;
  clienteForm: FormGroup;
  cliente_id: any;
  constructor(private cf: FormBuilder, private actiavatedRouter: ActivatedRoute, 
    private service: ServiceService, private router : Router) { }

  ngOnInit() {
    this.clienteForm = this.cf.group({
      nombre: new FormControl('', Validators.required),
      ap_paterno: new FormControl('', Validators.required),
      ap_materno: new FormControl(''),
      rfc: new FormControl('', Validators.required)
    });
    this.cliente_id = this.actiavatedRouter.snapshot.paramMap.get('id');
    console.log(this.cliente_id);
    this.getCliente();
  }

  getCliente() {
    this.service.getCliente(this.cliente_id).subscribe(cliente => {
      this.cliente = cliente;
      console.log(cliente, this.cliente);
    })
  }

  updateCliente() {
    console.log(this.cliente);
    this.service.putCliente(this.cliente, this.cliente_id).subscribe(resp => { this.router.navigate(['clientes']); })
  }

 

}
