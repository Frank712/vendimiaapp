import { Component, OnInit, ViewChild } from '@angular/core';
import { Config } from '../models/configuracionModel';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})

export class ConfiguracionComponent implements OnInit {
  config : any ;
  configForm : FormGroup;
  config_id : any;
  success : boolean = false;
  constructor(private cf : FormBuilder, private service : ServiceService) { }

  ngOnInit() {
    this.configForm = this.cf.group({
      tasa: new FormControl('', [Validators.pattern('^-?[0-9]\\d*(\\.\\d*)?$')]),
      enganche: new FormControl('', [Validators.pattern('^-?[0-9]\\d*(\\.\\d*)?$')]),
      plazo: new FormControl('', [Validators.pattern('^[0-9]*$')]),
    });
    this.getConfig();
  }

  getConfig(){
    this.service.getConfig().subscribe(config => {
      this.config = config;
    })
  }

  updateConfig(){
    this.config = this.saveConfig();
    console.log(this.config);
    this.service.putConfig(this.config, this.config_id).subscribe(resp => { this.success = true;})
  }

  saveConfig(){
    const saveConfig = {
      tasa : this.configForm.get('tasa').value,
      enganche : this.configForm.get('enganche').value,
      plazo : this.configForm.get('plazo').value
    }
    return saveConfig;
  }


}
