import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  URL = 'https://vendimiaapp-97c69.firebaseio.com/';
  constructor(private http : HttpClient) { }

  //************* VENTAS *****************
  postVenta(venta: any) {
    const newpress = JSON.stringify(venta);
    const URL = this.URL + 'ventas.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(URL, newpress, { headers });
  };

  getVentas() {
    const URL = this.URL + 'ventas.json';
    return this.http.get(URL);
  };

  //************* CLIENTES *****************
  postCliente(config: any) {
    const newpress = JSON.stringify(config);
    const URL = this.URL + 'clientes.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(URL, newpress, { headers });
  };
  getClientes() {
    const URL = this.URL + 'clientes.json';
    return this.http.get(URL);
  };
  getCliente(id : any) {
    const URL = this.URL + 'clientes/' + id + '.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(URL);
  };
  putCliente(cliente: any, id: string) {
    const newpress = JSON.stringify(cliente);
    const URL = this.URL + 'clientes/' + id + '.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(URL, newpress, { headers });
  };

  //************* ARTICULOS *****************
  postArticulo(articulo: any) {
    const newpress = JSON.stringify(articulo);
    const URL = this.URL + 'articulo.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(URL, newpress, { headers });
  };
  getArticulos() {
    const URL = this.URL + 'articulo.json';
    return this.http.get(URL);
  };
  getArticulo(id: any) {
    const URL = this.URL + 'articulo/' + id + '.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(URL);
  };
  putArticulo(articulo: any, id: string) {
    const newpress = JSON.stringify(articulo);
    const URL = this.URL + 'articulo/' + id + '.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(URL, newpress, { headers });
  };

  //************** CONFIGURACIONES *****************
  postConfig(config: any) {
    const newpress = JSON.stringify(config);
    const URL = this.URL + 'configuracion.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(URL, newpress, { headers });
  };
  putConfig(config: any, id: string) {
    const newpress = JSON.stringify(config);
    const URL = this.URL + 'configuracion.json';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(URL, newpress, { headers });
  };
  getConfig(){
    const URL = this.URL + 'configuracion.json';
    return this.http.get(URL);
  };

  
}
