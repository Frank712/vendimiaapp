import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ServiceService } from '../../service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-addarticulo',
  templateUrl: './addarticulo.component.html',
  styleUrls: ['./addarticulo.component.css']
})
export class AddarticuloComponent implements OnInit {
  articulo: any;
  articuloForm: FormGroup;
  articulo_id: any;
  constructor(private cf: FormBuilder, private actiavatedRouter: ActivatedRoute,
    private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.articuloForm = this.cf.group({
      descripcion: new FormControl('', Validators.required),
      modelo: new FormControl(''),
      precio: new FormControl('', [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d*)?$')]),
      existencia: new FormControl('', Validators.required)
    });
    this.articulo_id = this.actiavatedRouter.snapshot.paramMap.get('id');
  }

  postArticulo() {
    this.articulo = this.saveArticulo();
    console.log(this.articulo);
    this.service.postArticulo(this.articulo).subscribe(resp => {
      this.router.navigate(['articulos']);
    });
  };

  saveArticulo() {
    const saveConfig = {
      clave: this.articulo_id,
      descripcion: this.articuloForm.get('descripcion').value,
      modelo: this.articuloForm.get('modelo').value,
      precio: this.articuloForm.get('precio').value,
      existencia: this.articuloForm.get('existencia').value
    }
    return saveConfig;
  }

}
