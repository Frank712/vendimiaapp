import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.css']
})
export class ArticulosComponent implements OnInit {
  articulos: any[] = [];
  id : any;

  constructor(private service: ServiceService) { }

  ngOnInit() {
    this.service.getArticulos().subscribe(articulos => {
      console.log(articulos);
      for (const id$ in articulos) {
        const c = articulos[id$];
        c.id$ = id$;
        this.articulos.push(articulos[id$]);
      }
      this.id = Number(this.articulos.length) + 1;
    })

  }

}
