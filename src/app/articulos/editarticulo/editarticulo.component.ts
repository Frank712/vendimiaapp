import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ServiceService } from '../../service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editarticulo',
  templateUrl: './editarticulo.component.html',
  styleUrls: ['./editarticulo.component.css']
})
export class EditarticuloComponent implements OnInit {
  articulo: any;
  articuloForm: FormGroup;
  articulo_id: any;
  constructor(private cf: FormBuilder, private actiavatedRouter: ActivatedRoute,
    private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.articuloForm = this.cf.group({
      descripcion: new FormControl('', Validators.required),
      modelo: new FormControl(''),
      precio: new FormControl('', [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d*)?$')]),
      existencia: new FormControl('', Validators.required)
    });
    this.articulo_id = this.actiavatedRouter.snapshot.paramMap.get('id');
    console.log(this.articulo_id);
    this.getArticulo();
  }

  getArticulo() {
    this.service.getArticulo(this.articulo_id).subscribe(articulo => {
      this.articulo = articulo;
      console.log(articulo, this.articulo);
    })
  }

  updateArticulo() {
    console.log(this.articulo);
    this.service.putArticulo(this.articulo, this.articulo_id).subscribe(resp => { this.router.navigate(['articulos']); })
  }

}
