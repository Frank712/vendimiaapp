import { Component } from '@angular/core';
import localeMX from '@angular/common/locales/es-MX';
import { registerLocaleData } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'VendimiaApp';
  today : Date = new Date();

  constructor(){
    registerLocaleData(localeMX);
    setInterval(() => {
      this.today = new Date();
    }, 1);
  }
}
