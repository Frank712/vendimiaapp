import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {
  ventas: any[] = [];
  id: any;

  constructor(private service: ServiceService) { }

  ngOnInit() {
    this.service.getVentas().subscribe(ventas => {
      console.log(ventas);
      for (const id$ in ventas) {
        const c = ventas[id$];
        c.id$ = id$;
        this.ventas.push(ventas[id$]);
      }
      this.id = Number(this.ventas.length) + 1;
    })
  }
 
}
