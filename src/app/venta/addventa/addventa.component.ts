import { Component, OnInit, ElementRef, ViewChild, Renderer } from '@angular/core';
import { ServiceService } from 'src/app/service.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ActivatedRoute, Router } from '@angular/router';

export class Cliente {
  nombre;
  ap_paterno;
  ap_materno;
  fullname;
  clave;
  rfc;
  id$;
};

export class Articulo {
  clave;
  descripcion;
  modelo;
  precio;
  precio_o;
  cantidad : number = 1;
  existencia;
  importe;
  id$;
};

export class Plazo {
  total_pagar : number = 0;
  abono : number = 0;
  ahorro : number = 0;
}

@Component({
  selector: 'app-addventa',
  templateUrl: './addventa.component.html',
  styleUrls: ['./addventa.component.css']
})
export class AddventaComponent implements OnInit {
  clientes : Cliente [] = [];
  cliente : Cliente = new Cliente;
  articulos : Articulo [] = [];
  articulo : Articulo = new Articulo;
  articulo_venta: Articulo = new Articulo;
  venta_id : any = '';
  enganche : number = 0;
  bonificacion: number = 0;
  plazo: number = 0;
  etapa : number = 0;
  total: number = 0;
  venta : boolean = false;
  existencia_error: boolean = false;
  datos_error: boolean = false;
  plazos_error: boolean = false;
  config : any;
  clienteControl = new FormControl();
  articuloControl = new FormControl();
  clientesFiltered: Observable<Cliente[]>;
  articulosFiltered: Observable<Articulo[]>;
  contado : number = 0;
  plazo_3 : Plazo = new Plazo;
  plazo_6 : Plazo = new Plazo;
  plazo_9 : Plazo = new Plazo;
  plazo_12 : Plazo = new Plazo;

  

  constructor(private actiavatedRouter: ActivatedRoute,
    private service: ServiceService, private router: Router, private renderer: Renderer) { }

  ngOnInit() {
    this.getClientes();
    this.getArticulos();
    this.getConfig();
    this.clientesFiltered= this.clienteControl.valueChanges.pipe(
      startWith(''),
      map(value => this.cliente_filter(value))
    );
    this.articulosFiltered = this.articuloControl.valueChanges.pipe(
      startWith(''),
      map(value => this.articulo_filter(value))
    );
    this.venta_id = this.actiavatedRouter.snapshot.paramMap.get('id');
  }

  private cliente_filter(value: string): Cliente[] {
    const filterValue = value.toLowerCase();
    return this.clientes.filter(option => option.fullname.toLowerCase().includes(filterValue));
  };

  private articulo_filter(value: string): Articulo[] {
    const filterValue = value.toLowerCase();
    return this.articulos.filter(option => option.descripcion.toLowerCase().includes(filterValue));
  }

  selectCliente(cliente: any) {
    console.log(cliente);
    this.cliente = cliente;
    this.clienteList(cliente);
  };

  selectArticulo(articulo: any) {
    console.log(articulo);
    this.articulo = articulo;
    this.articuloList(articulo);
  }

  clienteList(data: any) {
    this.clientes = this.clientes.filter(option => option.clave == data.clave);
  }

  articuloList(data: any) {
    this.articulos= this.articulos.filter(option => option.clave == data.clave);
  }

  setArticuloVenta(){
    if (this.articulo.existencia > 0) {
      this.venta = true;
      this.articulo_venta = this.articulo;
      this.articulo_venta.cantidad = 1;
      this.articulo_venta.precio_o = this.articulo_venta.precio;
      this.articulo_venta.precio = this.articulo_venta.precio * (1 + (this.config.tasa * this.config.plazo) / 100);
      this.articulo_venta.importe = this.articulo_venta.precio * this.articulo_venta.cantidad;
      this.articulo = new Articulo;
      this.updateTotal();
    } else {
      this.existencia_error = true;
    }
  };

  updateImporte(){
    if (this.articulo_venta.cantidad <= this.articulo_venta.existencia){
      this.existencia_error = false;
      this.articulo_venta.importe = this.articulo_venta.precio * this.articulo_venta.cantidad;
      this.updateTotal();
    } else {
      this.existencia_error = true;
    }
  };

  updateTotal(){
    this.enganche = (this.config.enganche/100)*this.articulo_venta.importe;
    this.bonificacion = this.enganche * ((this.config.tasa * this.config.plazo)/100);
    this.total = this.articulo_venta.importe - this.enganche - this.bonificacion;
  };

  getAbonos(){
    if (this.articulo_venta.importe && this.cliente.id$){
      this.etapa = 1;
      this.datos_error = false;
      this.contado = this.total / (1 + ((this.config.tasa * this.config.plazo) / 100));
      this.plazo_3.total_pagar = this.contado * (1 + (this.config.tasa * 3) / 100);
      this.plazo_6.total_pagar = this.contado * (1 + (this.config.tasa * 6) / 100);
      this.plazo_9.total_pagar = this.contado * (1 + (this.config.tasa * 9) / 100);
      this.plazo_12.total_pagar = this.contado * (1 + (this.config.tasa * 12) / 100);
      this.plazo_3.abono = this.plazo_3.total_pagar / 3;
      this.plazo_6.abono = this.plazo_6.total_pagar / 6;
      this.plazo_9.abono = this.plazo_9.total_pagar / 9;
      this.plazo_12.abono = this.plazo_12.total_pagar / 12;
      this.plazo_3.ahorro = this.total - this.plazo_3.total_pagar;
      this.plazo_6.ahorro = this.total - this.plazo_6.total_pagar;
      this.plazo_9.ahorro = this.total - this.plazo_9.total_pagar;
      this.plazo_12.ahorro = this.total - this.plazo_12.total_pagar;
    }else{
      this.datos_error = true;
    }
    
  }

  deleteVenta(){
    this.venta = false;
    this.articulo_venta = new Articulo;
  }

  uploadVenta(){
    if(this.plazo>0){
      let venta = this.saveVenta();
      let articulo = this.saveArticulo();
      this.service.postVenta(venta).subscribe(resp => {
          this.service.putArticulo(articulo, this.articulo_venta.id$).subscribe(resp => { 
            this.router.navigate(['ventas']);
          })
      });
    } else {
      this.plazos_error = true;
    }
  };

  saveArticulo(){
    let existencia = this.articulo_venta.existencia - this.articulo_venta.cantidad;
    const articulo = {
      clave : this.articulo_venta.clave,
      descripcion : this.articulo_venta.descripcion,
      existencia : existencia,
      modelo : this.articulo_venta.modelo,
      precio : this.articulo_venta.precio_o
    };
    return articulo;
  }

  saveVenta(){
    let total;
    switch (Number(this.plazo)) {
      case 3: {
        total = this.plazo_3.total_pagar;
        break;
      }
      case 6: {
        total = this.plazo_6.total_pagar;
        break;
      }
      case 9: {
        total = this.plazo_9.total_pagar;
        break;
      }
      case 12: {
        total = this.plazo_12.total_pagar;
        break;
      }
      default: {
        break;
      }
    };
    let date = new Date();
    let string_date = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    const venta = {
      folio: this.venta_id,
      clave_cliente: this.cliente.clave,
      nombre_cliente: this.cliente.fullname,
      total: total,
      fecha: string_date
    };
    return venta;
  }

  getClientes(){
    this.service.getClientes().subscribe(clientes => {
      console.log(clientes);
      for (const id$ in clientes) {
        const c = clientes[id$];
        console.log(c, id$);
        c.id$ = id$;
        c.fullname = c.nombre + ' ' + c.ap_paterno + ' ' + c.ap_materno;        
        this.clientes.push(clientes[id$]);
      }
      console.log(this.clientes);
    });
    
  }

  getArticulos(){
    this.service.getArticulos().subscribe(articulos => {
      console.log(articulos);
      for (const id$ in articulos) {
        const c = articulos[id$];
        c.id$ = id$;
        this.articulos.push(articulos[id$]);
      }
    })
  };

  getConfig() {
    this.service.getConfig().subscribe(config => {
      this.config = config;
    })
  }

}
